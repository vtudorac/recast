# RECAST for Run2 4L SUSY analysis 
https://arxiv.org/abs/2103.11684


**Needs the following inputs**

input_mc16a/d/e: DAOD files in SUSY2 format (p3895 preferred)\
xsecpb: The cross section of the physics sample in pb\
PRWFILE_mc16: the pileup file for this sample\
signal_process: shorthand for the signal process, e.g. C1N2WZ_200_150\
use_susy_proc: 'si' if the sample is composed of multiple SUSY production processes. In this case, a xsec file will also be needed instead of a single cross-section (susy_xsec_file)


The workflow makes analysis ntuples using the XAMPP framework, followed by a tree with all the necessary systematics\
https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmultilep

The tree is then used as input to the 4L Histfitter setup to produce xml files\
https://gitlab.cern.ch/atlas-phys-susy-wg/Electroweak/4LAnalysis/histfittersetup

Finally, pyhf is used to calculate CLs values for each SR, choosing the best performing (best CLs exp) where overlaps exist to go into a combination of orthogonal regions. The final json file with the combined result is named recast_4LCombi_*signal_process*_fitresult.json
